// These code was made by: @nurkhaulah_rzka

'use strict';

const fs = require('fs');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', function (inputStdin) {
  inputString += inputStdin;
});

process.stdin.on('end', function () {
  inputString = inputString.split('\n');

  main();
});

function readLine() {
  return inputString[currentLine++];
}

/*
 * Complete the 'compareTriplets' function below.
 *
 * The function is expected to return an INTEGER_ARRAY.
 * The function accepts following parameters:
 *  1. INTEGER_ARRAY a
 *  2. INTEGER_ARRAY b
 */

function compareTriplets(a, b) {
  let scoreA = 0,
    scoreB = 0;

  for (let i = 0; i < a.length; i++) {
    if (a[i] > b[i]) {
      // jika index ke x array a lebih besar dari index ke x array b,
      // maka score a akan bertambah 1
      scoreA++;
    } else if (a[i] < b[i]) {
      // jika index ke x array a lebih kecil dari index ke x array b,
      // maka score a akan bertambah 1
      scoreB++;
    }
  }
  return [scoreA, scoreB];
}

// sample input
const sample = compareTriplets([17, 28, 30], [99, 16, 8]);
console.log(sample); // expected output [2, 1]

function main() {
  const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

  const a = readLine()
    .replace(/\s+$/g, '')
    .split(' ')
    .map((aTemp) => parseInt(aTemp, 10));

  const b = readLine()
    .replace(/\s+$/g, '')
    .split(' ')
    .map((bTemp) => parseInt(bTemp, 10));

  const result = compareTriplets(a, b);

  ws.write(result.join(' ') + '\n');
  s;
  ws.end();
}
